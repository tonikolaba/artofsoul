[![Build Status](https://travis-ci.org/tonikolaba/artofsoul.svg?branch=master)](https://travis-ci.org/tonikolaba/artofsoul)

# artofsoul

Your own template, independently artistic website. artofsoul is a full template web page, colored with magnificent visual style and excellent taste,
 beautiful colors and graphics, natural website navigation, and well presented information and products.
 It can be used for photographer, bloger, artist and the most important from anyone how fill a bit artist.

## Screenshot

Visit it: <https://tonikolaba.github.io/artofsoul/>
 
 
## Built With

* [Bootstarp](http://getbootstrap.com/getting-started/) - Bootstrap

Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web. 
Bootstrap makes front-end web development faster and easier. 
It's made for folks of all skill levels, devices of all shapes, and projects of all sizes.


## Authors

* **Toni Kolaba** - *Initial work* - [tonikolaba](https://github.com/tonikolaba)

![Alt text](https://github.com/tonikolaba/aos-ArtistWebpage/blob/master/img/ArtOfSoul.png?raw=true"ArtofSoul")



